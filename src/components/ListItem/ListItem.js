import React from 'react';
import { View, Text, StyleSheet, TouchableNativeFeedback} from 'react-native';


const ListItem = (props) =>(
  <TouchableNativeFeedback onPress={props.onItemPressed}>
    <View style={styles.ListItem}>
      <Text>{props.placeName} </Text>
    </View>
  </TouchableNativeFeedback>
);
 

const styles= StyleSheet.create({
  ListItem: {
    width: "100%",
    marginBottom: 5,
    padding: 10,
    backgroundColor: "#eee"
    
  }
})

export default ListItem;
