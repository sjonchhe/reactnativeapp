import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, Button} from 'react-native';
import PlaceInput from "./src/components/PlaceInput/PlaceInput";
import PlaceList from "./src/components/PlaceList/PlaceList";


export default class App extends Component {
  state = {
    places: []
    };

    placeAddedHandler = placeName => {
      this.setState(prevState => {
        return {
          places: prevState.places.concat({key: Math.random(), value: placeName})
        };
      });
    };
  
  placeDeletedHandler = index => {
    this.setState (prevState => {
      return {
        places: prevState.places.filter( place => {
          return place.key !== key;
        })
      }
    });
  }    
  render() {
  
    return (
      // <ImageBackground source={require('./src/assets/2.jpg')} >

          <View style={styles.container}>

              <PlaceInput onPlaceAdded={this.placeAddedHandler}/>
              <PlaceList places={this.state.places} onItemDeleted={this.placeDeletedHandler} />
            
          </View>

      // </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 26,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  image: {
    //width: '100%',
    //flex: 1,
    // alignItems: "center"
  }
  
});
